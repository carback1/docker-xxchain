#!/bin/sh


if [ ! -f /data/.initialized ]
then
    touch /data/.initialized
    rsync -avz /data-initial/ /data/
    echo "Data volume empty, initialized fresh state!"
fi

mkdir -p /var/log/supervisor

# Redirect to "syslog" file but keep printing out in this console as well
exec > >(tee -a "/data/syslog") 2>&1

echo "Starting supervisord..."
exec /usr/bin/supervisord -c /etc/supervisord.conf
