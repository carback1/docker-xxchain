FROM alpine:3.18
LABEL maintainer "Richard T. Carback III <rick@xx.network>"

RUN apk update --no-cache
RUN apk upgrade --no-cache

# Compile and install the xxnetwork chain binary
# https://users.rust-lang.org/t/unable-to-find-libclang-the-libclang-issue/70746
ENV LIBCLANG_PATH="/usr/lib/llvm14/lib"
# https://github.com/rust-lang/wg-cargo-std-aware/issues/70
# https://users.rust-lang.org/t/dynamic-linking-in-alpine/105608
ENV RUSTFLAGS="-Ctarget-feature=-crt-static -Lnative=/usr/lib"
ENV PATH="/root/.cargo/bin:$PATH"
# NOTE: These fix minor dep errors
#    cargo update proc-macro2 quote && \
#    cargo update ahash@0.7.6 --precise 0.7.8 && \
#    cargo update ahash@0.8.3 --precise 0.8.11 && \
RUN apk add build-base git go make cmake clang14 clang14-dev \
      perl linux-headers protoc --no-cache && \
    wget https://sh.rustup.rs && mv index.html rust.sh && chmod +x rust.sh && \
    ./rust.sh -y && \
    git clone --branch bridge --depth 1 https://git.xx.network/xx-labs/xxchain.git && \
    rustup toolchain install nightly && \
    rustup target add wasm32-unknown-unknown && \
    rustup target add --toolchain nightly wasm32-unknown-unknown && \
    rustup default nightly && \
    rustup update && \
    cd xxchain && \
    cargo update proc-macro2 quote && \
    cargo update ahash@0.7.6 --precise 0.7.8 && \
    cargo update ahash@0.8.3 --precise 0.8.11 && \
    make build-prod && \
    make test-pallets && \
    cd .. && \
    mv xxchain/target/production/xxnetwork-chain /usr/sbin/ && \
    rm -fr xxchain /root/.cargo /root/.cache /root/.rustup /rust.sh && \
    apk del build-base git go make cmake clang14 clang14-dev \
      perl linux-headers protoc --no-cache

# Configure supervisor for running inside a container
RUN apk add supervisor openssl rsync --no-cache
RUN mkdir -p /var/log/supervisor/

COPY supervisord.conf /etc/supervisord.conf
COPY start.sh /start.sh
RUN chmod +x *.sh

STOPSIGNAL SIGQUIT

# xxchain ports
EXPOSE 15974 63007 9933

RUN mkdir -p /data/xxnetwork
RUN ln -s /data/xxnetwork /root/.local
RUN mv /var/log /data/logs && ln -s /data/logs /var/log
RUN cp -r /data /data-initial

VOLUME /data
CMD [ "/start.sh" ]
